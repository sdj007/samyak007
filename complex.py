class mycomplex():
    def __init__(self,real=0,img=0):
       self.real = real
       self.img = img

    def add(self,c):
        real= self.real + c.real
        img = self.img  + c.img
        return mycomplex(real,img)

    def show(self):
        print(' {}+i{} '.format(self.real,self.img))
x=mycomplex(1,1)
x.show()
y=mycomplex(1,1)
y.show()
z=x.add(y)
z.show()              
              
