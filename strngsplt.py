def cstolot(cs):
    lot=[]
    for i in cs.split(';'):
        t= tuple(i.split('='))
        lot.append(t)
    return lot

def lottos(mylist):
    cs=''
    for k,v in mylist:
        cs +='{}={};'.format(k,v)
    return cs.strip(';')
    

def main():
    cs="a=b;c=d;e=f;g=h"
    lot= cstolot(cs)
    print(lot)
    mycs= lottos(lot)
    print(mycs)
    
main()    
    
